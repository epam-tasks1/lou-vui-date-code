﻿using System;
using System.Globalization;

namespace LouVuiDateCode
{
    public static class DateCodeGenerator
    {
        public static string GenerateEarly1980Code(uint manufacturingYear, uint manufacturingMonth)
        {
            if (manufacturingYear < 1980 || manufacturingYear > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth < 0 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            return string.Concat(manufacturingYear.ToString(CultureInfo.CurrentCulture)[^2..], manufacturingMonth.ToString(CultureInfo.CurrentCulture));
        }

        public static string GenerateEarly1980Code(DateTime manufacturingDate)
        {
            if (manufacturingDate.Year < 1980 || manufacturingDate.Year > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            return string.Concat(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[^2..], manufacturingDate.Month.ToString(CultureInfo.CurrentCulture));
        }

        public static string GenerateLate1980Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingYear < 1980 || manufacturingYear > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (manufacturingMonth < 0 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            if (!CheckFactoryCode(factoryLocationCode))
            {
                throw new ArgumentException("FactoryLocationCode is wrong.");
            }

            return string.Concat(manufacturingYear.ToString(CultureInfo.CurrentCulture)[^2..], manufacturingMonth.ToString(CultureInfo.CurrentCulture), factoryLocationCode.ToUpper(CultureInfo.CurrentCulture));
        }

        public static string GenerateLate1980Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate.Month < 0 || manufacturingDate.Month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (manufacturingDate.Year < 1980 || manufacturingDate.Year > 1989)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (!CheckFactoryCode(factoryLocationCode))
            {
                throw new ArgumentException("FactoryLocationCode is wrong.");
            }

            return string.Concat(manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[^2..], manufacturingDate.Month.ToString(CultureInfo.CurrentCulture), factoryLocationCode.ToUpper(CultureInfo.CurrentCulture));
        }

        public static string Generate1990Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingMonth < 0 || manufacturingMonth > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingMonth));
            }

            if (manufacturingYear < 1990 || manufacturingYear > 2006)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (!CheckFactoryCode(factoryLocationCode))
            {
                throw new ArgumentException("FactoryLocationCode is wrong.");
            }

            string factoringYear = manufacturingYear.ToString(CultureInfo.CurrentCulture)[^2..];
            string factoringMonth = manufacturingMonth.ToString(CultureInfo.CurrentCulture);

            if (factoringMonth.Length == 1)
            {
                factoringMonth = string.Concat("0", factoringMonth);
            }

            return string.Concat(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture), factoringMonth[0], factoringYear[0], factoringMonth[1], factoringYear[1]);
        }

        public static string Generate1990Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate.Month < 0 || manufacturingDate.Month > 12)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (manufacturingDate.Year < 1990 || manufacturingDate.Year > 2006)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (!CheckFactoryCode(factoryLocationCode))
            {
                throw new ArgumentException("FactoryLocationCode is wrong.");
            }

            string factoringYear = manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[^2..];
            string factoringMonth = manufacturingDate.Month.ToString(CultureInfo.CurrentCulture);

            if (factoringMonth.Length == 1)
            {
                factoringMonth = string.Concat("0", factoringMonth);
            }

            return string.Concat(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture), factoringMonth[0], factoringYear[0], factoringMonth[1], factoringYear[1]);
        }

        public static string Generate2007Code(string factoryLocationCode, uint manufacturingYear, uint manufacturingWeek)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingYear < 2007)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingYear));
            }

            if (!CheckWeekInYear((int)manufacturingYear, (int)manufacturingWeek))
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingWeek));
            }

            if (!CheckFactoryCode(factoryLocationCode))
            {
                throw new ArgumentException("FactoryLocationCode is wrong.");
            }

            string factoringYear = manufacturingYear.ToString(CultureInfo.CurrentCulture)[^2..];
            string factoringWeek = manufacturingWeek.ToString(CultureInfo.CurrentCulture);

            if (factoringWeek.Length == 1)
            {
                factoringWeek = string.Concat("0", factoringWeek);
            }

            return string.Concat(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture), factoringWeek[0], factoringYear[0], factoringWeek[1], factoringYear[1]);
        }

        public static string Generate2007Code(string factoryLocationCode, DateTime manufacturingDate)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            if (manufacturingDate.Year < 2007)
            {
                throw new ArgumentOutOfRangeException(nameof(manufacturingDate));
            }

            if (!CheckFactoryCode(factoryLocationCode))
            {
                throw new ArgumentException("FactoryLocationCode is wrong.");
            }

            int manufacturingWeek = ISOWeek.GetWeekOfYear(manufacturingDate);
            DateTime yearStart = ISOWeek.GetYearStart(manufacturingDate.Year);

            if (yearStart > manufacturingDate)
            {
                manufacturingDate = new DateTime(manufacturingDate.Year - 1, manufacturingDate.Month, manufacturingDate.Day);
            }

            string factoringYear = manufacturingDate.Year.ToString(CultureInfo.CurrentCulture)[^2..];
            string factoringWeek = manufacturingWeek.ToString(CultureInfo.CurrentCulture);

            if (factoringWeek.Length == 1)
            {
                factoringWeek = string.Concat("0", factoringWeek);
            }

            return string.Concat(factoryLocationCode.ToUpper(CultureInfo.CurrentCulture), factoringWeek[0], factoringYear[0], factoringWeek[1], factoringYear[1]);
        }

        public static bool CheckFactoryCode(string factoryCode)
        {
            if (string.IsNullOrEmpty(factoryCode))
            {
                throw new ArgumentNullException(nameof(factoryCode));
            }

            factoryCode = factoryCode.ToUpper(CultureInfo.CurrentCulture);

            string[] arrayOfCodes = new string[]
            {
                "A0", "A1", "A2", "AA", "AAS", "AH", "AN", "AR", "AS", "BA", "BJ", "BU", "DR", "DU", "DR", "DT", "CO", "CT",
                "CX", "ET", "FL", "LW", "MB", "MI", "NO", "RA", "RI", "SD", "SF", "SL", "SN", "SP", "SR", "TJ", "TH", "TR",
                "TS", "VI", "VX", "LP", "OL", "BC", "BO", "CE", "FO", "MA", "OB", "RC", "RE", "SA", "TD", "CA", "LO", "LB",
                "LM", "LW", "GI", "DI", "FA", "FC", "FH", "LA", "OS",
            };

            for (int i = 0; i < arrayOfCodes.Length; i++)
            {
                if (factoryCode == arrayOfCodes[i])
                {
                    return true;
                }
            }

            return false;
        }

        public static bool CheckWeekInYear(int year, int week)
        {
            int weekInYear = ISOWeek.GetWeeksInYear(year);
            if (week > weekInYear || week <= 0)
            {
                return false;
            }

            return true;
        }
    }
}
