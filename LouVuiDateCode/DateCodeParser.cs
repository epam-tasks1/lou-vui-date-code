﻿using System;
using System.Globalization;
using static LouVuiDateCode.CountryParser;

namespace LouVuiDateCode
{
    public static class DateCodeParser
    {
        public static void ParseEarly1980Code(string dateCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 3 && dateCode.Length != 4)
            {
                throw new ArgumentException("DateCode is wrong.");
            }

            if (uint.Parse(dateCode[..2], CultureInfo.CurrentCulture) < 80 || uint.Parse(dateCode[..2], CultureInfo.CurrentCulture) > 89)
            {
                throw new ArgumentException("DateCode contains the wrong year.");
            }

            if (uint.Parse(dateCode[2..], CultureInfo.CurrentCulture) <= 0 || uint.Parse(dateCode[2..], CultureInfo.CurrentCulture) > 12)
            {
                throw new ArgumentException("DateCode contains the wrong month.");
            }

            manufacturingYear = 1900 + uint.Parse(dateCode[..2], CultureInfo.CurrentCulture);
            manufacturingMonth = uint.Parse(dateCode[2..], CultureInfo.CurrentCulture);
        }

        public static void ParseLate1980Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 5 && dateCode.Length != 6)
            {
                throw new ArgumentException("DateCode is wrong.");
            }

            if (uint.Parse(dateCode[..2], CultureInfo.CurrentCulture) < 80 || uint.Parse(dateCode[..2], CultureInfo.CurrentCulture) > 89)
            {
                throw new ArgumentException("DateCode contains the wrong year.");
            }

            if (uint.Parse(dateCode[2..^2], CultureInfo.CurrentCulture) <= 0 || uint.Parse(dateCode[2..^2], CultureInfo.CurrentCulture) > 12)
            {
                throw new ArgumentException("DateCode contains the wrong month.");
            }

            factoryLocationCountry = GetCountry(dateCode[^2..]);

            if (factoryLocationCountry.Length == 0)
            {
                throw new ArgumentException("DateCode contains the wrong factory code.");
            }

            factoryLocationCode = dateCode[^2..];
            manufacturingYear = 1900 + uint.Parse(dateCode[..2], CultureInfo.CurrentCulture);
            manufacturingMonth = uint.Parse(dateCode[2..^2], CultureInfo.CurrentCulture);
        }

        public static void Parse1990Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingMonth)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 6)
            {
                throw new ArgumentException("DateCode is wrong.");
            }

            if (uint.Parse(string.Concat(dateCode[3], dateCode[5]), CultureInfo.CurrentCulture) >= 7 && uint.Parse(string.Concat(dateCode[3], dateCode[5]), CultureInfo.CurrentCulture) < 90)
            {
                throw new ArgumentException("DateCode contains the wrong year.");
            }

            if (uint.Parse(string.Concat(dateCode[2], dateCode[4]), CultureInfo.CurrentCulture) <= 0 || uint.Parse(string.Concat(dateCode[2], dateCode[4]), CultureInfo.CurrentCulture) > 12)
            {
                throw new ArgumentException("DateCode contains the wrong month.");
            }

            factoryLocationCountry = GetCountry(dateCode[..2]);

            if (factoryLocationCountry.Length == 0)
            {
                throw new ArgumentException("DateCode contains the wrong factory code.");
            }

            factoryLocationCode = dateCode[..2];

            if (uint.Parse(string.Concat(dateCode[3], dateCode[5]), CultureInfo.CurrentCulture) >= 90)
            {
                manufacturingYear = 1900 + uint.Parse(string.Concat(dateCode[3], dateCode[5]), CultureInfo.CurrentCulture);
            }
            else
            {
                manufacturingYear = 2000 + uint.Parse(string.Concat(dateCode[3], dateCode[5]), CultureInfo.CurrentCulture);
            }

            manufacturingMonth = uint.Parse(string.Concat(dateCode[2], dateCode[4]), CultureInfo.CurrentCulture);
        }

        public static void Parse2007Code(string dateCode, out Country[] factoryLocationCountry, out string factoryLocationCode, out uint manufacturingYear, out uint manufacturingWeek)
        {
            if (string.IsNullOrEmpty(dateCode))
            {
                throw new ArgumentNullException(nameof(dateCode));
            }

            if (dateCode.Length != 6)
            {
                throw new ArgumentException("DateCode is wrong.");
            }

            uint factoryYear = 2000 + uint.Parse(string.Concat(dateCode[3], dateCode[5]), CultureInfo.CurrentCulture);
            uint factoryWeek = uint.Parse(string.Concat(dateCode[2], dateCode[4]), CultureInfo.CurrentCulture);

            if (factoryYear < 2007)
            {
                throw new ArgumentException("DateCode contains the wrong year.");
            }

            if (!CheckWeekInYear(factoryYear, factoryWeek))
            {
                throw new ArgumentException("DateCode contains the wrong weeks in year.");
            }

            factoryLocationCountry = GetCountry(dateCode[..2]);

            if (factoryLocationCountry.Length == 0)
            {
                throw new ArgumentException("DateCode contains the wrong factory code.");
            }

            factoryLocationCode = dateCode[..2];
            manufacturingYear = factoryYear;
            manufacturingWeek = factoryWeek;
        }

        public static bool CheckWeekInYear(uint year, uint week)
        {
            if (week == 0)
            {
                return false;
            }

            if (week <= ISOWeek.GetWeeksInYear((int)year))
            {
                return true;
            }

            return false;
        }
    }
}
