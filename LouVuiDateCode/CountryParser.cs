﻿using System;
using System.Collections.Generic;

namespace LouVuiDateCode
{
    public static class CountryParser
    {
        public static Country[] GetCountry(string factoryLocationCode)
        {
            if (string.IsNullOrEmpty(factoryLocationCode))
            {
                throw new ArgumentNullException(nameof(factoryLocationCode));
            }

            string[] codeFrance = new string[] { "A0", "A1", "A2", "AA", "AAS", "AH", "AN", "AR", "AS", "BA", "BJ", "BU", "DR", "DU", "DR", "DT", "CO", "CT", "CX", "ET", "FL", "LW", "MB", "MI", "NO", "RA", "RI", "SD", "SF", "SL", "SN", "SP", "SR", "TJ", "TH", "TR", "TS", "VI", "VX" };
            string[] codeGermany = new string[] { "LP", "OL" };
            string[] codeItaly = new string[] { "BC", "BO", "CE", "FO", "MA", "OB", "RC", "RE", "SA", "TD" };
            string[] codeSpain = new string[] { "CA", "LO", "LB", "LM", "LW", "GI" };
            string[] codeSwitzerland = new string[] { "DI", "FA" };
            string[] codeUSA = new string[] { "FC", "FH", "LA", "OS", "SD", "FL" };

            List<Country> factoryCountry = new List<Country>();

            CheckCountryCode(codeFrance, factoryLocationCode, factoryCountry, Country.France);
            CheckCountryCode(codeGermany, factoryLocationCode, factoryCountry, Country.Germany);
            CheckCountryCode(codeItaly, factoryLocationCode, factoryCountry, Country.Italy);
            CheckCountryCode(codeSpain, factoryLocationCode, factoryCountry, Country.Spain);
            CheckCountryCode(codeSwitzerland, factoryLocationCode, factoryCountry, Country.Switzerland);
            CheckCountryCode(codeUSA, factoryLocationCode, factoryCountry, Country.USA);

            return factoryCountry.ToArray();
        }

        public static void CheckCountryCode(string[] codeCountry, string factoryLocationCode, List<Country> factoryCountry, Country country)
        {
            if (codeCountry is null)
            {
                throw new ArgumentNullException(nameof(codeCountry));
            }

            if (factoryCountry is null)
            {
                throw new ArgumentNullException(nameof(factoryCountry));
            }

            for (int i = 0; i < codeCountry.Length; i++)
            {
                if (codeCountry[i] == factoryLocationCode)
                {
                    factoryCountry.Add(country);
                    break;
                }
            }
        }
    }
}
